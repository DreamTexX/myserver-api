FROM node:14.7.0-slim

WORKDIR /app/temp/

COPY package*.json ./
COPY tsconfig.json ./
COPY package*.json /app/api/

ADD images/ /images/
ADD src/ ./src/

RUN npm install
RUN npm run build
RUN cp -r out/* /app/api

WORKDIR /app/api/
RUN rm -rf /app/temp/

ENV NODE_ENV=production
RUN npm install

EXPOSE 16801
EXPOSE 13204

CMD [ "npm", "run", "prod" ]