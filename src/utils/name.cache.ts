export class NameCache {
    private static names: Map<string, string> = new Map<string, string>(); // <name, uuid>

    public static getUUID(name: string) {
        return this.names.get(name) || undefined;
    }

    public static getName(uuid: string) {
        return Array.from(this.names.entries()).filter(({ 1: v}) => v === uuid).map(([k]) => k)[0] || undefined;
    }

    public static put(name: string, uuid: string) {
        this.names.set(name.toLowerCase(), uuid);
    }
}