import { throws } from "assert";

export default class Bytes {

    private static units = {
        k: Math.pow(1024, 1),
        m: Math.pow(1024, 2),
        g: Math.pow(1024, 3),
        t: Math.pow(1024, 4),
        p: Math.pow(1024, 5),
        e: Math.pow(1024, 6),
        z: Math.pow(1024, 7),
        y: Math.pow(1024, 8),
    }
    private static unitsReable = [
        'B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'
    ]

    public static toBytes(literal: string): number {
        literal = literal.toLowerCase();
        if (!literal.match(/^[0-9]+(k|m|g|t|p)$/i))
            if (Number(literal) === NaN)
                return null;
            else
                return Number(literal);
        const unit = literal.charAt(literal.length - 1);
        return parseInt(literal.substring(0, literal.length - 1)) * this.units[unit];
    }

    public static fromBytes(bytes: number): string {
        const index = Math.floor(Math.log(bytes) / Math.log(1024));
        return (bytes / Math.pow(1024, index)).toFixed(2) + this.unitsReable[index];
    }
}