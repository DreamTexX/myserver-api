import { Request, Response } from 'express';
import { LoggerConnector } from "../connector/logger.connector";
import { DockerConnector } from "../connector/docker.connector";
import { ImageInfo } from "dockerode";
import ImageEntity from '../entity/image.entity';
import Bytes from '../utils/bytes';
import { v4 } from 'uuid';
import * as Docker from 'dockerode';
import * as Modem from 'docker-modem';
import { WebSocketConnector } from '../connector/websocket.connector';

export class ImageController {
    public static async list(req: Request, res: Response, next: Function) {
        try {
            const images: ImageInfo[] = await DockerConnector.getConnection().listImages({
                'filters': {
                    'label': [
                        'net.melion.myserver=true',
                    ],
                }
            });
            res.send({
                status: 'ok',
                images: images.map(image => {
                    return {
                        tag: image.RepoTags[0],
                        javaVersion: image.Labels['net.melion.myserver.image.javaVersion'],
                        mcVersion: image.Labels['net.melion.myserver.image.mcVersion'],
                        ramLowerLimit: Bytes.fromBytes(parseFloat(image.Labels['net.melion.myserver.image.ramLowerLimit'])),
                        ramMonitoringLimit: Bytes.fromBytes(parseFloat(image.Labels['net.melion.myserver.image.ramMonitoringLimit'])),
                        created: image.Created,
                    }
                })
            });
        } catch (err) {
            next(err);
        }
    }

    public static async addImage(req: Request, res: Response, next: Function) {
        if (!req.body.imageVersion || !req.body.ramLowerLimit || !req.body.ramMonitoringLimit ||
            !req.body.javaVersion || !req.body.mcVersion) {
            next({ error: 'Insufficient arguments, please provide "imageVersion", "ramLowerLimit", "ramMonitoringLimit", "javaVersion" and "mcVersion"', error_code: 'insufficient_arguments' });
            return;
        }

        let ramLowerLimit = Bytes.toBytes(req.body.ramLowerLimit);
        let ramMonitoringLimit = Bytes.toBytes(req.body.ramMonitoringLimit);
        if (!ramLowerLimit || !ramMonitoringLimit) {
            next({ error: 'Please provice "ramMemoryLimit" and "ramMonitoringLimit" with the right format as a number (bytes) or with number + k|m|g|t|p', error_code: 'invalid_arguments' });
            return;
        }

        if (!req.body.imageVersion.match(/^[0-9]+\.[0-9]+$/)) {
            next({ error: 'Please provide the image version in the following foramt: majorVersion.minorVersion', error_code: 'invalid_arguments' });
            return;
        }

        if (!req.body.mcVersion.match(/(^[0-9]+\.[0-9]+$)|(^[0-9]+\.[0-9]*\.[0-9]+)$/)) {
            next({ error: 'Please provide the minecraft version in the following foramt: majorVersion.minorVersion[.buildVersion]', error_code: 'invalid_arguments' });
            return;
        }

        const javaVersion = Number(req.body.javaVersion);
        if (javaVersion === NaN) {
            next({ error: 'Java version must be a number.', error_code: 'invalid_arguments' });
            return;
        }

        const buildId = v4();
        const docker: Docker = DockerConnector.getConnection();

        console.log("Passed data validation");

        try {
            const stream: NodeJS.ReadableStream = await docker.buildImage({ context: '/images', src: ['Dockerfile'] }, {
                t: 'myserver:' + req.body.imageVersion,
                buildargs: {
                    JAVA_VERSION: javaVersion.toString(),
                    MC_VERSION: req.body.mcVersion.toString(),
                },
                networkmode: 'host',
                labels: {
                    'net.melion.myserver': 'true',
                    'net.melion.myserver.image.ramLowerLimit': ramLowerLimit.toString(),
                    'net.melion.myserver.image.ramMonitoringLimit': ramMonitoringLimit.toString(),
                    'net.melion.myserver.image.javaVersion': req.body.javaVersion.toString(),
                    'net.melion.myserver.image.mcVersion': req.body.mcVersion.toString(),
                }
            });
            console.log("Passed image build start");
            let handled: boolean = false;
            (<Modem>docker.modem).followProgress(stream, (err, _) => {
                if (handled)
                    return;
                if (err)
                    WebSocketConnector.broadcast({ action: 'image_building_failed', uuid: buildId, details: { message: err.message, stack: err.stack, name: err.name } });
                else
                    WebSocketConnector.broadcast({ action: 'image_built', uuid: buildId });
            }, (data) => {
                if (data.error) {
                    WebSocketConnector.broadcast({ action: 'image_building_failed', uuid: buildId, details: data });
                    handled = true;
                    return;
                }

                const message: string = data.stream;
                if (!message || !message.startsWith("Step"))
                    return;
                const progress: number = parseInt(message.substr(5, message.indexOf('/')));
                const complete: number = parseInt(message.substr(message.indexOf('/') + 1, message.indexOf(':') - 1));
                WebSocketConnector.broadcast({ action: 'image_built_progress', uuid: buildId, progress: progress, complete: complete });
            });
            res.send({ status: 'ok', buildId: buildId });
        } catch (err) {
            next(err);
        }
    }

    public static async build(req: Request, res: Response, next: Function) {
        /**
         * Required Args in Docker:
         * --build-arg JAVA_VERSION
         * --build-arg MC_VERSION
         * 
         * 
         * 
         * TYP  | VERSION | RAM   | GRENZE | JAVA | ANMERKUNGEN
         * FREE |         |       |        |      |
         *      | 1. 8    | 400mb | 800mb  | 8    |
         *      | 1. 9    | 400mb | 800mb  | 8    |
         *      | 1.10    | 400mb | 800mb  | 8    |
         *      | 1.11    | 400mb | 800mb  | 8    |
         *      | 1.12    | 400mb | 800mb  | 11   |
         *      | 1.13    | 400mb | 1gb    | 11   |
         * PAID |         |       |        |      |
         *      | 1.14    | 800mb | 1.3g   | 14   | Weltgeneration dauert relativ lange
         *      | 1.15    | ---
         *      | 1.16    | ---
         */
    }
} 