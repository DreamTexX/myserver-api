import { Request, Response } from 'express';
import * as jwt from 'jsonwebtoken'

export class AuthController {
    public static async login(req: Request, res: Response, next: Function) {
        if (!req.body.server_id || !req.body.plugin_id) {
            res.send({error: 'Insufficient arguments, please provide "server_id" and "plugin_id".', error_code: 'insufficient_arguments'});
            return;
        }
        
        const token = jwt.sign({ server_id: req.body.server_id, plugin_id: req.body.plugin_id }, process.env.JWT_SECRET);
        res.send({token: token});
    }
}