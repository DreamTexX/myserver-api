import { Request, Response } from 'express';
import { MyServer } from '../model/myserver.model';
import { NameCache as NameCache } from '../utils/name.cache';

export class MyServerController {
    public static async startOrCreate(req: Request, res: Response, next: Function) {
        if (!req.body.uuid || !req.body.ram) {
            next({ error: 'Insufficient arguments, please provide "uuid" and "ram"', error_code: 'insufficient_arguments' });
            return;
        }

        const ram = parseInt(req.body.ram) * 1024 * 1024;
        try {
            if (await MyServer.getServer(req.body.uuid, ram).start())
                res.send({ status: "ok" });
            else
                res.send({ status: "failed" });
        } catch (err) {
            res.send(err);
        }
    }

    public static async stop(req: Request, res: Response, next: Function) {
        if (!req.body.uuid) {
            next({ error: 'Insufficient arguments, please provide "uuid"', error_code: 'insufficient_arguments' });
            return;
        }

        MyServer.getServer(req.body.uuid).stop(); // Ignore the Promise, stop is called detached an the request should not wait
        res.send({ status: 'ok' });
    }

    public static async status(req: Request, res: Response, next: Function) {
        if (!req.body.uuid && !req.body.name) {
            next({ error: 'Insufficient arguments, please provide "uuid" or "name"', error_code: 'insufficient_arguments' });
            return;
        }

        let uuid = req.body.uuid || NameCache.getUUID(req.body.name);
        if (!uuid) {
            try {
                uuid = (await (await fetch("https://api.mojang.com/users/profiles/minecraft/" + req.body.name)).json())['id'];
                NameCache.put(req.body.name, uuid);
            } catch (error) {
                next({ error: 'The provided name is not a valid minecraft user', erro_code: 'invalid_name' });
                return;
            }
        }

        const myServer: MyServer = MyServer.getServer(uuid);

        res.send({
            status: 'ok',
            port: await myServer.getPort(),
            running: await myServer.isRunning(),
        });
    }
}