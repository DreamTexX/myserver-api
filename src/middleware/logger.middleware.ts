import { Request, Response } from 'express';
import uuid = require('uuid');

export default (req: Request, res: Response, next: Function) => {
    const requestId = uuid.v1();
    req.headers.id = requestId;
    console.time('req-' + requestId);
    res.on('finish', () => {
        console.timeLog('req-' + requestId, '|', res.statusCode, '|', req.url);
    });
    next();
}