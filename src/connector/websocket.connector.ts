import { Server,  } from 'ws';
import { LoggerConnector } from './logger.connector';

export class WebSocketConnector {
    public static wss: Server;

    public static init() {
        this.wss = new Server({
            port: 13204,
            path: "/myserver/listener",
        });
        this.wss.on("connection", () => {
            LoggerConnector.logger.info("Client connected to websocket");
        })
    }

    public static broadcast(message: any) {
        LoggerConnector.logger.info("Broadcast:", message);
        this.wss.clients.forEach(client => {
            client.send(JSON.stringify(message));
        });
    }
}