import * as Docker from 'dockerode';
import { LoggerConnector } from './logger.connector';

export class DockerConnector {
    public static getConnection(): Docker {
        let client = undefined;
        LoggerConnector.logger.info("Obtaining a connection to docker...");
        try {
            client = new Docker();
            LoggerConnector.logger.info("Connected to local docker socket");
        } catch (error) {
            console.error(error);
            console.error("Connection failed. Please retry later");
            throw new Error("connecting to docker deamon faild");
        }

        return client;
    }
}