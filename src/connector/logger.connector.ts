import * as winston from 'winston';

export class LoggerConnector {
    public static logger: winston.Logger;

    public static init() {
        this.logger = winston.createLogger({
            level: 'silly',
            transports: [
                new winston.transports.File({
                    dirname: 'logs',
                    filename: 'combined.log',
                    level: 'info',
                    format: winston.format.combine(
                        winston.format.timestamp(),
                        winston.format.json(),
                    )
                }),
                new winston.transports.Console({
                    format: winston.format.combine(
                        winston.format.align(),
                        winston.format.colorize(),
                        winston.format.simple(),
                    )
                })
            ]
        });
    }
}