import * as mongoose from 'mongoose';
import { LoggerConnector } from './logger.connector';

export class DatabaseConnector extends mongoose.Connection {

    public static async init() {
        const connection: mongoose.Connection = (await mongoose.connect(`mongodb://mongodb:27017/myserver`, {
            useNewUrlParser: true,
            useFindAndModify: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
        })).connection;
        connection.on('error', err => {
            LoggerConnector.logger.error(err);
        })
        connection.once('open', () => {
            LoggerConnector.logger.info("Database connection etablished");
        })
    }
}