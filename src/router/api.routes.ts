import { AuthController } from '../controller/auth.controller';
import { MyServerController } from '../controller/myserver.controller';
import { ImageController } from '../controller/image.controller';

export const Routes = [
    {
        path: "/auth/login",
        method: "post",
        httpAction: AuthController.login,
        auth: false,
    }, {
        path: "/myserver/start",
        method: "post",
        httpAction: MyServerController.startOrCreate,
        auth: true,
    }, {
        path: "/myserver/stop",
        method: "post",
        httpAction: MyServerController.stop,
        auth: true,
    }, {
        path: "/myserver/status",
        method: "get",
        httpAction: MyServerController.status,
        auth: true,
    }, {
        path: "/image/list",
        method: "get",
        httpAction: ImageController.list,
        auth: true,
    }, {
        path: "/image",
        method: "post",
        httpAction: ImageController.addImage,
        auth: true,
    }
];