import { Request, Response, NextFunction, Router } from "express";
import { Routes } from "./api.routes";
import { httpAuthenticate } from "../middleware/auth.middleware";

const ApiRouter = Router();
Routes.forEach(route => {
    if (route.method != "ws") {
        ApiRouter[route.method]
            (route.path, (route.auth ? httpAuthenticate : (_1, _2, next: NextFunction) => next), (request: Request, response: Response, next: NextFunction) => {
                route.httpAction(request, response, next)
                    .then(() => next)
                    .catch(err => next(err));
            });
    } else {
        
    }
});

export default ApiRouter;