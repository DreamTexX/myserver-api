import { Schema, Mongoose, Document, model } from 'mongoose';
import { DockerConnector } from '../connector/docker.connector';
import { LoggerConnector } from '../connector/logger.connector';

const ImageSchema = new Schema({
    imageVersion: {
        type: String,
        required: true,
        unique: true,
    },
    ramLowerLimit: {
        type: Number,
        required: true
    },
    ramMonitoringLimit: {
        type: Number,
        required: true,
    },
    javaVersion: {
        type: String,
        required: true,
    },
    mcVersion: {
        type: String,
        required: true,
    }
});

ImageSchema.methods.exists = function () {
    try {
        DockerConnector.getConnection().getImage(`myserver:${this.imageVersion}`);
        return true;
    } catch (err) {
        return false;
    }
}

ImageSchema.methods.build = async function () {
    const stream: NodeJS.ReadableStream = await DockerConnector.getConnection().buildImage({ context: '/images', src: ['Dockerfile'] }, {
        t: 'myserver:' + this.imageVersion,
        buildargs: {
            JAVA_VERSION: this.javaVersion,
            MC_VERSION: this.mcVersionh,
        },
        networkmode: 'host',
    });
    stream.eventNames().forEach(eventName => {
        stream.on(eventName, data => {
            LoggerConnector.logger.debug(`${eventName.toString()}: ${data}`);
        });
    });
}

export interface IImage extends Document {
    imageVersion: string;
    ramLowerLimit: number;
    ramMonitoringLimit: number;
    javaVersion: string;
    mcVersion: string;
}

export default model<IImage>('Image', ImageSchema);