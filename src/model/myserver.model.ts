import { DockerConnector } from "../connector/docker.connector";
import * as Docker from 'dockerode';
import { LoggerConnector } from "../connector/logger.connector";
import { WebSocketConnector } from "../connector/websocket.connector";
import { runInThisContext } from "vm";

export class MyServer {
    public static myServers: Map<string, MyServer> = new Map<string, MyServer>();

    public static getServer(uuid: string, ram?: number): MyServer {
        return this.myServers.get(uuid) || new MyServer(uuid, ram);
    }

    /*public static async getAllServers(): MyServer[] {
        let docker: Docker = DockerConnector.getConnection();
        const containers: Docker.ContainerInfo[] = await docker.listContainers({
            all: true,
            'filters': {
                'label': [
                    'net.melion.myserver=true',
                    'net.melion.myserver.uuid=' + this.uuid,
                ]
            }
        });
    }*/

    private uuid: string;
    private ram: number;
    private docker: Docker;
    private container: Docker.Container;
    private logHandlers: { (message: string): void; }[] = [];
    private openStreams: NodeJS.ReadWriteStream[] = [];
    
    constructor(uuid: string, ram: number) {
        if (MyServer.myServers.get(uuid))
            throw new Error("Theres already an instance of this Server. Please make sure you are using this!");

        this.uuid = uuid;
        this.ram = ram;
        MyServer.myServers.set(this.uuid, this);

        // Initialize connection to docker
        this.docker = DockerConnector.getConnection();
    }

    public attachLogHandler(handler: { (message: string): void; }) {
        this.logHandlers.push(handler);
    }

    public detachLogHandler(handler: { (message: string): void; }) {
        this.logHandlers = this.logHandlers.filter(h => h !== handler);
    }

    private async attach() {
        /* CLOSE OPENED STREAMS AND OPEN ONE */
        this.openStreams.forEach((stream: NodeJS.ReadWriteStream) => {
            stream.pause();
        })

        this.container.attach({ stream: true, stdout: true, stderr: true }, (err, stream: NodeJS.ReadWriteStream) => {
            if (err) {
                LoggerConnector.logger.info(err);
                return;
            }
            this.openStreams.push(stream);
            stream.on("data", (message: Buffer) => {
                this.logHandlers.forEach(h => h(message.toString('utf-8')));
                if (message.toString('utf-8').match(/INFO]: Done \(.*s\)! For help, type "help"/g)) {
                    WebSocketConnector.broadcast({ action: 'loaded', uuid: this.uuid });
                }
            });
        });

        this.docker.getEvents({
            'filters': {
                'label': [
                    'net.melion.myserver=true',
                    'net.melion.myserver.uuid=' + this.uuid,
                ]
            }
        }, (error, stream: NodeJS.ReadableStream) => {
            LoggerConnector.logger.info("Connected to event pipe");
            if (error)
                LoggerConnector.logger.info(error);
            stream.on("data", (message: Buffer) => {
                LoggerConnector.logger.debug(message.toString('utf-8'));
                const data = JSON.parse(message.toString('utf-8'));

                if (data.status === "stop" || data.status === "destroy" || data.status === "die") {
                    this.container = undefined;
                    this.openStreams.forEach((stream: NodeJS.ReadWriteStream) => {
                        stream.pause();
                    });
                    stream.unpipe();
                    stream.pause();
                }
            });
        });
    }

    public async start(restart = false): Promise<boolean> {
        /* CHECK FOR USERS DATA */
        const volumes: Docker.VolumeInspectInfo[] = await this.docker.listVolumes({
            'filters': {
                'label': [
                    'net.melion.myserver=true',
                    'net.melion.myserver.uuid=' + this.uuid,
                ]
            }
        }).then(data => {
            if (data.Warnings)
                LoggerConnector.logger.warn(data.Warnings);
            return data.Volumes;
        });

        let volumeName = undefined;
        if (volumes.length == 0) {
            /* CREATE DATA VOLUME IF THERE IS NONE */
            try {
                const volume: Docker.VolumeInspectInfo = await this.docker.createVolume({
                    Labels: {
                        'net.melion.myserver': 'true',
                        'net.melion.myserver.uuid': this.uuid,
                    }
                })
                volumeName = volume.Name;
            } catch (err) {
                LoggerConnector.logger.error("There was an error while creating the data volume:", err);
                return false;
            }
        } else {
            /* USE FIRTST RETURNED DATA VOLUME RETURNED */
            volumeName = volumes[0].Name;
        }

        /* CHECK FOR RUNNING CONTAINERS */
        const containers: Docker.ContainerInfo[] = await this.docker.listContainers({
            'filters': {
                'label': [
                    'net.melion.myserver=true',
                    'net.melion.myserver.uuid=' + this.uuid,
                ]
            }
        });

        /* STOP EVERY RUNNING CONTAINER IF RESTART IS WANTED OR CLEAN UP CONTAINERS */
        if (restart || containers.length > 1) {
            for (let index = restart ? 0 : 1; index < containers.length; index++) {
                const container = this.docker.getContainer(containers[index].Id);
                try {
                    container.remove();
                } catch (err) {
                    LoggerConnector.logger.error(err);
                }
            }
        }

        if (containers.length > 0 && !restart) {
            /* THERE IS ALREADY AN CONTAINER RUNNING, ATTACH AND USE */
            this.container = this.docker.getContainer(containers[0].Id);
            await this.attach();
            return true;
        }

        /* CREATE TEMPORARY CONTAINER FROM IMAGE */
        this.container = await this.docker.createContainer({
            Image: 'myserver:1.16.1',
            Labels: {
                'net.melion.myserver': 'true',
                'net.melion.myserver.uuid': this.uuid,
            },
            ExposedPorts: {
                '25565/tcp': {},
            },
            HostConfig: {
                AutoRemove: true,
                NetworkMode: 'myserver_network',
                Memory: this.ram || 1024 * 1024 * 1024, //default ram to 1G if needed
                PortBindings: {
                    '25565/tcp': [{
                        'HostIP': '0.0.0.0',
                        'HostPort': undefined,
                    }]
                },
                Binds: [
                    volumeName + ':/data:rw'
                ]
            }
        });

        /* RUN THE CONTAINER */
        try {
            await this.container.start();
        } catch (error) {
            /* Just catch and ignore the error if the server is already running */
            if (error.statusCode != 304) {
                LoggerConnector.logger.info(error);
                return false;
            }
        }

        await this.attach();
        return true;
    }

    public async stop() {
        const containers: Docker.ContainerInfo[] = await this.docker.listContainers({
            'filters': {
                'label': [
                    'net.melion.myserver=true',
                    'net.melion.myserver.uuid=' + this.uuid,
                ]
            }
        });

        /* CLEANUP CONTAINERS */
        for (let index = 0; index < containers.length; index++) {
            const container = this.docker.getContainer(containers[index].Id);
            try {
                await container.stop();
            } catch (err) {
                LoggerConnector.logger.error(err);
            }
        }

        return;
    }

    public async getPort(): Promise<number> {
        if (!this.container)
            return -1;
        const containerInfo: Docker.ContainerInspectInfo = await this.container.inspect();
        return parseInt(containerInfo.NetworkSettings.Ports['25565/tcp'][0]['HostPort']);
    }

    public async isRunning(): Promise<boolean> {
        if (!this.container)
            return false;
        const containerInfo: Docker.ContainerInspectInfo = await this.container.inspect();
        return containerInfo.State.Running;
    }
}