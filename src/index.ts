import { Request, Response } from 'express';
import ApiRouter from './router/api.router';
import loggerMiddleware from './middleware/logger.middleware';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import { WebSocketConnector } from './connector/websocket.connector';
import { DockerConnector } from './connector/docker.connector';
import { LoggerConnector } from './connector/logger.connector';


LoggerConnector.init();
LoggerConnector.logger.info("Application is starting...");

WebSocketConnector.init();
//DatabaseConnector.init();

const app = express();
const server = require("http").Server(app);

DockerConnector.getConnection().getEvents({
    filters: {
        type: ['container'],
        label: ['net.melion.myserver=true']
    }
}, (error, stream: NodeJS.ReadableStream) => {
    LoggerConnector.logger.info("Connected to event pipe");
    if (error) {
        LoggerConnector.logger.info(error);
        return;
    }
    stream.on("data", (message: Buffer) => {
        const data = JSON.parse(message.toString('utf-8'));
        WebSocketConnector.broadcast({
            action: data.status,
            uuid: data.Actor.Attributes["net.melion.myserver.uuid"]
        });
    });
});

app.use(loggerMiddleware);
app.use(bodyParser.json());
app.use('/api/v1', ApiRouter);
app.all('/info', (req: Request, res: Response) => {
    let dockerStatus = 'healthy';
    try {
        DockerConnector.getConnection();
    } catch (ignored) {
        dockerStatus = 'unhealthy';
    }
    res.send({
        message: 'MyServer-API',
        status: {
            websocket: WebSocketConnector.wss ? 'healthy' : 'unhealthy',
            docker: dockerStatus
        },
    });
});
app.use((err: Error, req: Request, res: Response, next: express.NextFunction) => {
    LoggerConnector.logger.error('Unhandled error:', err);
    res.send({ error: 'Internal server error', error_code: 'internal', details: { message: err.message, stack: err.stack, name: err.name } });
});

server.listen(16801);
LoggerConnector.logger.info("Application is running.");
